This guide contains the essential steps necessary to integrate Shibboleth IdP v3.3+ and the MFA handler with CAS as fundamental authentication.  This guide will result in Shibboleth prompting the user for Duo authentication.  If you would like CAS to prompt, please see `casduo.md`.

These steps may impact your existing implementation.  Since these servers can be configured in a thousand ways, it's impossible to document all possible points of contact.  If you have questions, please contact the Chancellor's Office or an appropriate open source list.

It is presumed that you already have a functional Shibboleth environment and CAS environment, but that the two have not been integrated nor configured for MFA.

Please download the Shibcas distribution from [https://github.com/Unicon/shib-cas-authn3/releases](https://github.com/Unicon/shib-cas-authn3/releases).

**If you would like Shibboleth to prompt the user, follow these steps.**

1.  Copy the files from `Shibcas` to the corresponding directories.

        cp -r edit-webapp /opt/shibboleth-idp/edit-webapp
        cp -r flows /opt/shibboleth-idp/flows

2.  Change the authentication mechanism to MFA in `/opt/shibboleth-idp/conf/idp.properties`.

        idp.authn.flows= MFA

3.  Add Shibcas properties to `idp.properties`.  These will depend on your CAS server.  **Be sure to know whether your CAS server supports the CAS 3.0 protocol.**

        shibcas.serverName = https://idp.calstate.edu
        shibcas.casServerUrlPrefix = https://cas.calstate.edu/cas
        shibcas.casServerLoginUrl = ${shibcas.casServerUrlPrefix}/login
        #If your CAS server only supports CAS 2.0, force the plugin to downversion
        #shibcas.ticketValidatorName = cas20

4.  Create a rule in your CAS environment to release at least a principal name to the IdP as you would for any other application.

5.  Modify `/opt/shibboleth-idp/conf/authn/general-authn.xml` to declare the right principal types for the right mechanisms.  This is an excerpt.

        <bean id="authn/Shibcas" parent="shibboleth.AuthenticationFlow"
                p:passiveAuthenticationSupported="true"
                p:forcedAuthenticationSupported="true"
                p:nonBrowserSupported="false">
            <property name="supportedPrincipals">
                <list>
                    <bean parent="shibboleth.SAML2AuthnContextClassRef"
                        c:classRef="urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport" />
                </list>
            </property>
        </bean>

        <bean id="authn/Duo" parent="shibboleth.AuthenticationFlow"
                p:forcedAuthenticationSupported="true"
                p:nonBrowserSupported="false">
            <property name="supportedPrincipals">
                <list>
                    <bean parent="shibboleth.SAML2AuthnContextClassRef"
                        c:classRef="https://iam.calstate.edu/csuconnect/mfa" />
                </list>
            </property>
        </bean>
        
        <bean id="authn/MFA" parent="shibboleth.AuthenticationFlow"
                p:passiveAuthenticationSupported="true"
                p:forcedAuthenticationSupported="true">
            <!--
            The list below almost certainly requires changes, and should generally be the
            union of any of the separate factors you combine in your particular MFA flow
            rules. The example corresponds to the example in mfa-authn-config.xml that
            combines IPAddress with Password.
            -->
            <property name="supportedPrincipals">
                <list>
                    <bean parent="shibboleth.SAML2AuthnContextClassRef"
                        c:classRef="urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport" />
                    <bean parent="shibboleth.SAML2AuthnContextClassRef"
                        c:classRef="https://iam.calstate.edu/csuconnect/mfa" />
                </list>
            </property>
        </bean>

6.  Add `idp.duo.integrationKey`, `idp.duo.applicationKey`, `idp.duo.apiHost`, and `idp.duo.secretKey` properties to either `/opt/shibboleth-idp/conf/authn/duo.properties` or `/opt/shibboleth-idp/conf/sensitive.properties`.  Ensure that this properties file is referenced by idp.properties.

        idp.duo.integrationKey=
        idp.duo.applicationKey=
        idp.duo.apiHost=api-12345678.duosecurity.com
        idp.duo.secretKey=

7.  Define basic MFA routing in your IdP.  You can get fancy if you prefer, but this is the core.

        <util:map id="shibboleth.authn.MFA.TransitionMap">
            <!-- First rule runs the Shibcas login flow. -->
            <entry key="">
                <bean parent="shibboleth.authn.MFA.Transition" p:nextFlow="authn/Shibcas" />
            </entry>
            
            <!--
            Second rule runs a function if Shibcas succeeds, to determine whether an additional
            factor is required.
            -->
            <entry key="authn/Shibcas">
                <bean parent="shibboleth.authn.MFA.Transition" p:nextFlowStrategy-ref="checkSecondFactor" />
            </entry>

            <!-- An implicit final rule will return whatever the final flow returns. -->
        </util:map>

        <!-- Script to check if Duo is required.  Duo configuration parameters are in sensitive.properties. -->
        <bean id="checkSecondFactor" parent="shibboleth.ContextFunctions.Scripted" factory-method="inlineScript">
            <constructor-arg>
                <value>
                <![CDATA[
                    nextFlow = "authn/Duo";
                    authCtx = input.getSubcontext("net.shibboleth.idp.authn.context.AuthenticationContext");
		    		mfaCtx = authCtx.getSubcontext("net.shibboleth.idp.authn.context.MultiFactorAuthenticationContext");
		    		if (mfaCtx.isAcceptable()) {
		        		nextFlow=null;
		        		}
                    nextFlow;   // pass control to second factor or end with the first
                ]]>
                </value>
            </constructor-arg>
        </bean>

7.  Rebuild the `.war` using a build script for your platform in `/opt/shibboleth-idp/bin/build.*`.  Restart your servlet container.

8.  You can test by going to [https://sp.test.calstate.edu/mfatest](https://sp.test.calstate.edu/mfatest) and selecting your identity provider.

9.  You can test with CFS MFA by going to [https://login.calstate.edu/csuconnect/cfsmfa.asp?env=FCFSPRD](https://login.calstate.edu/csuconnect/cfsmfa.asp?env=FCFSPRD).