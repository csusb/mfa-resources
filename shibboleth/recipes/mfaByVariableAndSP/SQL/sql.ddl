/*
** This DDL for for Microsoft SQL Server
**
** principalName stores the principalName for a user returned by the IdP after authentication; 
** modify the data type and size of this column to match the unique identifier for your users in your identity system.
**
** entityID is the Entity ID of the SP that requires Multi-Factor Authentication
**
** Example:
**
** principalName entityID                       creationTimestamp
** ------------- ------------------------------ -----------------------
** jdoe          https://login.calstate.edu/mfa 2017-06-06 13:52:19.997
**
** There are two other special cases that you can add data for:
**
** 1. A service that all users will need to use MFA using a single record.
** 2. A user that will use MFA with all services using a single record.
** 
** The first case is implemented by using a specific reserved principalName
** (in the example below we use the principalName 'all-users') that is not ever 
** assigned to any user and adding a row with that special principalName
** and the entityID:
**
** principalName entityID                               creationTimestamp
** ------------- -------------------------------------- -----------------------
** all-users     https://specialservice.example.edu/mfa 2017-06-06 13:52:19.997
**
** To complete this, you must modify the attribute resolver configuration to always
** include rows with the principalName of all-users. The example attribute-resolver.xml
** configuration file already includes this.
**
** The second case is implemented by using a specific reserved entityID (in the example
** below we use https://all-services/mfa) and adding a row using the user's principalName
** combined with the special entityID:
**
** principalName entityID                 creationTimestamp
** ------------- ------------------------ -----------------------
** jsmith        https://all-services/mfa 2017-06-06 13:52:19.997
**
** The mfa-script.xml must be modified to look for this entityID so it will use MFA regardless
** of the entityID of the SP.
**
*/
CREATE TABLE IdP_PersonServiceMFA (
	principalName NVARCHAR(256) NOT NULL,
	entityID NVARCHAR(256) NOT NULL,
	creationTimestamp DATETIME NULL
 CONSTRAINT PK_PersonServiceMFA PRIMARY KEY CLUSTERED 
(
	principalName ASC,
	entityID ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
) ON PRIMARY
GO
ALTER TABLE IdP_PersonServiceMFA ADD  CONSTRAINT DF_PersonServiceMFA_CreationTimestamp  DEFAULT (GETDATE()) FOR creationTimestamp
GO
