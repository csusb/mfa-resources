Given an LDAP data connector that is able to retrieve membership in a specific group as an LDAP attribute and uid, the following attribute definition may be added to attribute-resolver.xml.  If the user's uid is present in the group, then the boolean needsDuo will flip to true.

The attribute may then be used in an mfa configuration script at conf/authn/mfa-authn-config.xml, released to the service provider as an audit stamp, or used in another context by the IdP.

There is no error checking in this, so all data must be present at runtime.  That means the two preconditions are having a value for uid (typically a given if they could authenticate) and successful retrieval of the attribute used to represent membership in that group.